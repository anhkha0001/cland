package vn.edu.vinaenter.defines;

public class MessagesDefine {
	public static final String MSG_SUCCESS = "Thêm thành công";
	public static final String MSG_SUCCESS_DELETE = "Xóa thành công";
	public static final String MSG_SUCCESS_UPDATE = "Sửa thành công";
	public static final String MSG_ERROR = "Có lỗi trong quá trình xử lý ";
}
