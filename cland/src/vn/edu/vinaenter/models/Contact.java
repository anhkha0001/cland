package vn.edu.vinaenter.models;

public class Contact {
private int cId;
private String fullName;
private String email;
private String subject;
private String content;
public Contact() {
	super();
	// TODO Auto-generated constructor stub
}
public int getcId() {
	return cId;
}
public void setcId(int cId) {
	this.cId = cId;
}
public String getFullName() {
	return fullName;
}
public void setFullName(String fullName) {
	this.fullName = fullName;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getSubject() {
	return subject;
}
public void setSubject(String subject) {
	this.subject = subject;
}
public String getContent() {
	return content;
}
public void setContent(String content) {
	this.content = content;
}
public Contact(int cId, String fullName, String email, String subject, String content) {
	super();
	this.cId = cId;
	this.fullName = fullName;
	this.email = email;
	this.subject = subject;
	this.content = content;
}

}
