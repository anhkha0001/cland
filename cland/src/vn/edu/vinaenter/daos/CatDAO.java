package vn.edu.vinaenter.daos;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import vn.edu.vinaenter.models.Category;

@Repository
public class CatDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	public List<Category> getListCat(){
		String sqlString = "SELECT * FROM `categories`";
		return jdbcTemplate.query(sqlString, new BeanPropertyRowMapper<Category>(Category.class));
	}
	public Category getCatById(int id) {
		String sql= "SELECT * FROM `categories` WHERE cid = ?";
		return jdbcTemplate.queryForObject(sql, new Object[] {id},new BeanPropertyRowMapper<Category>(Category.class));
	}
	
	public int deleteCatById(int id) {
		String sql = "DELETE FROM `categories` WHERE `categories`.`cid` = ?";
		return jdbcTemplate.update(sql,new Object[] {id});
	}
	public int updateCat(Category category) {
		String sqlString = "UPDATE categories SET cname = ? WHERE categories.cid = ?";
		return jdbcTemplate.update(sqlString,new Object[] {category.getcName(),category.getcId()});
	}
	public int addCat(Category category) {
		String sqlString = "INSERT INTO categories (cid, cname) VALUES (NULL, ?);";
		return jdbcTemplate.update(sqlString,new Object[] {category.getcName()});
	}
		
}
