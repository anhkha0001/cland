package vn.edu.vinaenter.controller.publics;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PublicIndexController {
	
	@GetMapping("")
	public String index() {
		return "public.index";
	}
}
