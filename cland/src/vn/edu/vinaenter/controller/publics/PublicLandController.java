package vn.edu.vinaenter.controller.publics;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Repository("cland")
public class PublicLandController {

	@GetMapping("cat")
	public String cat(ModelMap modelMap) {
		return "public.cat";
	}
	
	@GetMapping("detail")
	public String detail(ModelMap modelMap) {
		return "public.detail";
	}
}
