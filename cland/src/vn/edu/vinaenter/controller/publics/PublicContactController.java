package vn.edu.vinaenter.controller.publics;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("contact")
public class PublicContactController {
	
	@GetMapping
	public String contact(ModelMap modelMap) {
		return "public.contact";
	}
}
