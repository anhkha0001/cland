package vn.edu.vinaenter.controller.admins;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("admin/land")
public class AdminLandController {
		
	@GetMapping
	public String index(ModelMap modelMap) {
		return "admin.land.index";
	}
	
	@GetMapping("add")
	public String add(ModelMap modelMap) {
		return "admin.land.add";
	}
	
}
