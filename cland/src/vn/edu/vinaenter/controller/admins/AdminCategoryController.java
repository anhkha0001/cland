package vn.edu.vinaenter.controller.admins;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sun.xml.internal.ws.api.model.wsdl.editable.EditableWSDLBoundFault;

import vn.edu.vinaenter.daos.CatDAO;
import vn.edu.vinaenter.defines.MessagesDefine;
import vn.edu.vinaenter.models.Category;

@Controller
@RequestMapping("admin/cat")
public class AdminCategoryController {

	@Autowired
	private CatDAO catDAO;
	@GetMapping
	public String index(ModelMap modelMap) {
		modelMap.addAttribute("listCat",catDAO.getListCat());
		return "admin.cat.index";
	}
	@GetMapping("delete/{id}")
	public String delete(RedirectAttributes redirectAttributes,@PathVariable("id") int id) {
		if(catDAO.deleteCatById(id) >0) {
			redirectAttributes.addAttribute("msg", MessagesDefine.MSG_SUCCESS_DELETE);
			return "redirect:/admin/cat";
		}
		else {
			redirectAttributes.addAttribute("msgErr",MessagesDefine.MSG_ERROR);
			return "redirect:/admin/cat";
		}
	}
	@GetMapping("edit/{id}")
	public String edit(@PathVariable("id") int id,ModelMap modelMap) {
		modelMap.addAttribute("cat", catDAO.getCatById(id));
		return "admin.cat.edit";
	}
	@PostMapping("edit")
	public String EditableWSDLBoundFault(@ModelAttribute("objCat") Category category, RedirectAttributes redirectAttributes) {
		if(catDAO.updateCat(category)>0) {
			redirectAttributes.addFlashAttribute("msg",MessagesDefine.MSG_SUCCESS_UPDATE);
		return "redirect:/admin/cat";
		}
		else {
			redirectAttributes.addFlashAttribute("msg",MessagesDefine.MSG_ERROR);
			return "admin.cat.edit";
			
		}
	}
	
	@PostMapping("add")
	public String add(@ModelAttribute("objCat") Category category,RedirectAttributes redirectAttributes) {
		if(catDAO.addCat(category) >0) {
			redirectAttributes.addFlashAttribute("msg",MessagesDefine.MSG_SUCCESS);
			return "redirect:/admin/cat";
		}
		else {
			redirectAttributes.addFlashAttribute("msg",MessagesDefine.MSG_ERROR);
			return "admin.cat.add";
		}
	}
	@GetMapping("add")
	public String add() {
		return "admin.cat.add";
	}
}
