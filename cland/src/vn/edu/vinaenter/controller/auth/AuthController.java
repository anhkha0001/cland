package vn.edu.vinaenter.controller.auth;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller("cpanel")
public class AuthController {
	
	@GetMapping("login")
	public String login(){
		return "auth.login";
	}
}
