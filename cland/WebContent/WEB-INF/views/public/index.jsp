<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div class="clearfix slider">
						<%@include file="/WEB-INF/templates/public/slide.jsp" %>
					</div>
					<div class="clearfix content">
						<div class="content_title"><h2>Bài viết mới</h2></div>
						
						<div class="clearfix single_content">
							<div class="clearfix post_date floatleft">
								<div class="date">
									<h3>27</h3>
									<p>Tháng 3</p>
								</div>
							</div>
							<div class="clearfix post_detail">
								<h2><a href="">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. </a></h2>
								<div class="clearfix post-meta">
									<p><span><i class="fa fa-clock-o"></i> Địa chỉ: Phạm Như Xương - Đà Nẵng</span> <span><i class="fa fa-folder"></i> Diện tích: 100m2</span></p>
								</div>
								<div class="clearfix post_excerpt">
									<img src="${contextPath}/resources/public/images/thumb.png" alt=""/>
									<p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a 
									ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class 
									aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos </p>
								</div>
								<a href="">Đọc thêm</a>
							</div>
						</div>
						
						<div class="clearfix single_content">
							<div class="clearfix post_date floatleft">
								<div class="date">
									<h3>27</h3>
									<p>Tháng 3</p>
								</div>
							</div>
							<div class="clearfix post_detail">
								<h2><a href="">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. </a></h2>
								<div class="clearfix post-meta">
									<p><span><i class="fa fa-clock-o"></i> Địa chỉ: Phạm Như Xương - Đà Nẵng</span> <span><i class="fa fa-folder"></i> Diện tích: 100m2</span></p>
								</div>
								<div class="clearfix post_excerpt">
									<img src="${contextPath}/resources/public/images/thumb.png" alt=""/>
									<p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a 
									ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class 
									aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos </p>
								</div>
								<a href="">Đọc thêm</a>
							</div>
						</div>
						
						<div class="clearfix single_content">
							<div class="clearfix post_date floatleft">
								<div class="date">
									<h3>27</h3>
									<p>Tháng 3</p>
								</div>
							</div>
							<div class="clearfix post_detail">
								<h2><a href="">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. </a></h2>
								<div class="clearfix post-meta">
									<p><span><i class="fa fa-clock-o"></i> Địa chỉ: Phạm Như Xương - Đà Nẵng</span> <span><i class="fa fa-folder"></i> Diện tích: 100m2</span></p>
								</div>
								<div class="clearfix post_excerpt">
									<img src="${contextPath}/resources/public/images/thumb.png" alt=""/>
									<p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a 
									ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class 
									aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos </p>
								</div>
								<a href="">Đọc thêm</a>
							</div>
						</div>						
					</div>
					
					<div class="pagination">
						<nav>
							<ul>
								<li><a href=""> << </a></li>
								<li><a href="">1</a></li>
								<li><a href="">2</a></li>
								<li><a href="">3</a></li>
								<li><a href="">4</a></li>
								<li><a href=""> >> </a></li>
							</ul>
						</nav>
					</div>